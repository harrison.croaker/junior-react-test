# Junior React Developer Test

## Overview
This test is designed to evaluate your skills in React, API integration, and user interface development. The task is to create a simple React application that allows users to select an HTTP status code and displays a corresponding image of a dog from the provided API. Additionally, there is a bonus feature to randomly generate a status code and fetch the corresponding dog image.

## Requirements

**User Interface**
- Create a dropdown or a list of options for users to select an HTTP status code (list provided in statusCodes.json)
- Display the selected status code prominently on the page.
- Implement a button or a trigger to fetch the dog image based on the selected status code.

**API Integration**
- Use the provided API (https://http.dog/) to fetch the dog image corresponding to the selected status code.

**Image Display**
- Display the fetched dog image on the page.
- Implement appropriate styling and layout for the image.

**Code Quality**
- Follow best practices for React component structure and organization.
- Use appropriate state management techniques (e.g., React hooks, context API).
- Write clean, readable, and well-documented code.
- Implement error handling and provide user-friendly error messages.

**Bonus Points**
- Random Status Code Generation
  - Add a button or trigger to randomly generate an HTTP status code.
  - Upon clicking the button, generate a random status code within a valid range (e.g., 100-599).
  - Fetch and display the corresponding dog image for the randomly generated status code.

**Submission**
- To submit your test, please provide the following:
  - A link to a GitHub repository containing your React project.
  - Or an email to harrison.croaker@localsearch.com.au with the zipped code repo
